/*
 * pcg64.c -- random number generator slow path
 */

#include "config.h"

#include <err.h>
#include <fcntl.h>
#include <stdint.h>
#ifdef HAVE_SYS_RANDOM_H
#include <sys/random.h>
#endif
#include <unistd.h>

#include "pcg64.h"

void
pcg64_getentropy(pcg64_t *rng) {
#ifdef HAVE_GETRANDOM
	if(getrandom(rng, sizeof(*rng), 0) < 0)
		err(1, "getrandom");
#else
        int fd = open("/dev/urandom", O_RDONLY);
        if(fd < 0) err(1, "open /dev/urandom");
        ssize_t n = read(fd, rng, sizeof(*rng));
        if(n < (ssize_t)sizeof(*rng))
		err(1, "read /dev/urandom");
        close(fd);
#endif
	rng->inc |= 1;
}

/*
 * Regenerate `num` if it is one of `residue = (1 << 64) % limit` biased
 * values, so that the return value is sampled from `(1 << 64) - residue
 * == N * limit` random values, for the largest possible `N`.
 */
uint64_t
pcg64_limit_slow(pcg64_t *rng, uint64_t limit, uint128_t num) {
	uint64_t residue = (uint64_t)(-limit) % limit;
	while(uint128_lo64(num) < residue)
		num = (uint128_t)pcg64(rng) * (uint128_t)limit;
        return(uint128_hi64(num));
}
