/*
 * The pcg64 random number generator is from https://www.pcg-random.org/
 *
 * Copyright (c) 2014-2017 Melissa O'Neill and PCG Project contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Daniel Lemire's nearly-divisionless algorithm for unbiased
 * bounded random numbers, https://lemire.me/blog?p=17551
 *
 * He says, "Unless otherwise stated, I make no copyright claim
 * on this code: you may consider it to be in the public domain."
 * https://github.com/lemire/Code-used-on-Daniel-Lemire-s-blog
 *
 * This version adapted by Tony Finch <dot@dotat.at>
 */

#ifndef PCG64_H
#define PCG64_H

#define unlikely(e) __builtin_expect((e), 0)

typedef __uint128_t uint128_t;

#define uint128_lo64(u) ((uint64_t)(u))
#define uint128_hi64(u) ((uint64_t)((u) >> 64))

typedef struct pcg64 {
        uint128_t state;
        uint128_t inc;
} pcg64_t;

/* initializer */
void pcg64_getentropy(pcg64_t *rng);

/*
 * This multiplier is from Pierre L'Ecuyer, "Tables of Linear Congruential
 * Generators of Different Sizes and Good Lattice Structure" (1999),
 * https://www.iro.umontreal.ca/~lecuyer/papers.html
 * The expression calculates 47026247687942121848144207491837523525
 */
#define PCG_MULTIPLIER_128 ((uint128_t)2549297995355413924ULL << 64 | \
			    (uint128_t)4865540595714422341ULL)

static inline uint64_t
pcg64(pcg64_t *rng) {
	uint128_t state = rng->state;
	/* linear congruential generator */
        rng->state = state * PCG_MULTIPLIER_128 + rng->inc;
	/* permuted output */
        uint64_t xsl = uint128_lo64(state) ^ uint128_hi64(state);
        uint64_t rr = uint128_lo64(state >> 122);
        return((xsl >> (+rr & 63)) | (xsl << (-rr & 63)));
}

/*
 * Slowly but accurately check if num is biased, and regenerate it until
 * it is not. Returns an unbiased random integer less than limit.
 */
uint64_t pcg64_limit_slow(pcg64_t *rng, uint64_t limit, uint128_t num);

/*
 * Get an unbiased random integer less than limit. Nearly always fast.
 *
 * Treat pcg64() as the low half of a 64.64 fixed-point number, so
 * it is a random number less than one, and multiply to get a 64.64
 * fixed-point random number less than the limit. See Donald Knuth's
 * Art of Computer Programming, volume 2, 3rd edition, section 3.6,
 * point (vi) on page 185.
 *
 * Rather than simply truncating as Knuth suggests, use Daniel Lemire's
 * nearly-divisionless algorithm to debias the result. The limit is a
 * slight over-estimate for the correct threshold, which can be
 * expensive to calculate, so we do that in the slow path.
 */
static inline uint64_t
pcg64_limit(pcg64_t *rng, uint64_t limit) {
        uint128_t num = (uint128_t)pcg64(rng) * (uint128_t)limit;
        if(unlikely(uint128_lo64(num) < limit))
		return(pcg64_limit_slow(rng, limit, num));
        return(uint128_hi64(num));
}

#endif /* PCG64_H */
